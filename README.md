# Machine Learning

A lo largo del notebook, se han tratado de completar los diferentes puntos
que hemos visto en las clases y que se pide en la practica.

En primer lugar, antes de si quiera cargar los datos practicamente, se filtran los
datos de la ciudad de Madrid ya que son los requeridos para el desarrollo de la practica.

Posteriormente, se procede a cargar los datos y dividirlos en train y test. El objetivo es
que sólo se utilicen los datos de train para realizar en análisis exploratorio y la selección de variables.

En los siguientes pasos se realiza en análisis exploratorio de los datos en el que se analizan 
los diferentes tipos de variables (numéricas, categoricas...) se analizan las distribuciones, completitud(%nulos),
correlaciones etc. Dependiendo del tipo se eliminan, se completan etc.

Una vez tenemos realizado el análisis, se procede a codificar las variables categóricas, mediante el algoritmo MeanEncoder.

Antes de comenzar a ejecutar algoritmos trabajaremos con Lasso y Random Forest para realizar un filtrado de variables, evaluar la importancia
de cada una de ellas y tener el resultado del dataset final.

Finalmente, se realizan pruebas de los algoritmos con los datos totales y filtrados para dar la mejor solución posible al problema de regresión.
